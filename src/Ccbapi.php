<?php

namespace Brv\Ccbapi;

class Ccbapi
{
    public $credentials = array('jeremy','jpapi15');
    public $base_uri = 'https://brv.ccbchurch.com';

    public function __construct()
    {
        // Create default HandlerStack
        $stack = HandlerStack::create();

		    // Add this middleware to the top with `push`
		    $stack->push(new CacheMiddleware(), 'cache');


		    $this->ccbClient = new Client([
			      'base_uri' => $this->base_uri,
			      'auth' => $this->credentials,
			      'Accept' => 'application/xml',
	    	]);

	}

	public function checkUser()
	{
		$response = $this->ccbClient->post('api.php', ['query'=>['srv'=>'individual_profile_from_login_password','login'=>$_REQUEST['user'],'password'=>$_REQUEST['pass']]]);
		$xmlResult = simplexml_load_string($response->getBody()->getContents());
		if (isset($xmlResult->response->individuals->individual->full_name)) {
			return true;
		} else {
			return false;
		}
	}

	public function getGroups()
	{
		$response = $this->ccbClient->get('api.php', ['query'=>['srv'=>'group_profiles','include_participants'=>false,'include_image_link'=>false]]);
		$xmlResult = simplexml_load_string($response->getBody()->getContents());
		
		$groups = array();

		foreach($xmlResult->response->groups AS $group)
		{
			
			foreach($group AS $groupData)
			{
				$groups[(int) $groupData->attributes()->id] = $groupData->name;
			}
		}

		return $groups;
	}

	public function get_participants($groupId)
	{
		$response = $this->ccbClient->get('api.php',['query'=>['srv'=>'group_participants','id'=>$groupId]]);
		
		$xmlResult = simplexml_load_string($response->getBody()->getContents());

		$participantIds = array();
		// var_dump($xmlResult);
		// var_dump($xmlResult->response->groups->group->participants);
		foreach($xmlResult->response->groups->group->participants AS $participants) {
			// var_dump($participant->participant);
			foreach ($participants->participant AS $participant) {
				// var_dump($participant);
				// var_dump((int) $participant->attributes()->id);
				$participantIds[] = (int) $participant->attributes()->id;
			}
			
		}
		$jsonResult = json_encode($xmlResult);
		$result = json_decode($jsonResult);
		$participants = $result->response->groups->group->participants->participant;
		// var_dump($participantIds);
		foreach ($participants AS $key => &$participant) {
			$participant->id = $participantIds[$key];
		}
		// var_dump($participants);
		return $participants;
	}

	public function get_availability($personId) {
		
		$response = $this->ccbClient->get('api.php',['query'=>['srv'=>'individual_availability','id'=>$personId]]);
		$xmlResult = simplexml_load_string($response->getBody()->getContents());
		$jsonResult = json_encode($xmlResult);
		$result = json_decode($jsonResult)->response;
		
		$tmp = [];
		
		$tmp['name'] = $result->individuals->individual->full_name;
		if (isset($result->individuals->individual->unavailable_dates->date))
		$tmp['unavailable'] = $result->individuals->individual->unavailable_dates->date;
		if (isset($result->individuals->individual->accepted_requests->date))
		$tmp['accepted_requests'] = $result->individuals->individual->accepted_requests->date;
		

		return $tmp;
	}

	public function getSundays($month)
	{
	    return new \DatePeriod(
	        new \DateTime("first sunday of $month"),
	        \DateInterval::createFromDateString('next sunday'),
	        new \DateTime("last day of $month")
	    );
	}

	public function getSaturdays($month)
	{
	    return new \DatePeriod(
	        new \DateTime("first saturday of $month"),
	        \DateInterval::createFromDateString('next saturday'),
	        new \DateTime("last day of $month")
	    );
	}

	public function getMonths($rangeStart, $rangeEnd)
	{
		$start    = (new \DateTime($rangeStart))->modify('first day of this month');
		$end      = (new \DateTime($rangeEnd))->modify('first day of next month');
		$interval = \DateInterval::createFromDateString('1 month');
		$period   = new \DatePeriod($start, $interval, $end);

		$months = array();
		foreach ($period as $dt) {
		    $months[] = $dt->format('Y-m');
		}

		return $months;
	}

	public function get_group_availability($groupId)
	{
		
		$participants = $this->get_participants($groupId);
		$availability = ['participants' => $participants];

		$months = $this->getMonths($_REQUEST['rangeStart'],$_REQUEST['rangeEnd']);
		$availMap = array();

		foreach ($months as $month) {
			foreach($this->getSaturdays($month) as $saturday) {
				$availMap[] = $saturday->format('Y-m-d');
			}

			foreach($this->getSundays($month) as $sunday) {
				$availMap[] = $sunday->format('Y-m-d');
			}
		}

		$availMap = array_flip($availMap);
		ksort($availMap);

		foreach ($participants AS $participant) {
			$avail = $this->get_availability($participant->id);

			
			if (isset($avail['unavailable']) && is_array($avail['unavailable'])) {
				foreach ($avail['unavailable'] AS $unavailableDate) {
					if (array_key_exists($unavailableDate, $availMap)) {
						if (!is_array($availMap[$unavailableDate]))
							$availMap[$unavailableDate] = array('unavailable'=>array());
						if (!is_array($availMap[$unavailableDate]['unavailable']))
							$availMap[$unavailableDate]['unavailable'] = array();
						$availMap[$unavailableDate]['unavailable'][] = $participant->id;
					}
				}
			}
			

			reset($availMap);
			if (isset($avail['accepted_requests']) && is_array($avail['accepted_requests'])) {
				foreach ($avail['accepted_requests'] AS $acceptedDate) {
					$date = new \DateTime($acceptedDate);
					$acceptedDate = $date->format('Y-m-d');
					if (array_key_exists($acceptedDate, $availMap)) {
						if (!is_array($availMap[$acceptedDate]))
							$availMap[$acceptedDate] = array('accepted' => array());
						if (!is_array($availMap[$acceptedDate]['accepted'])) {
							$availMap[$acceptedDate]['accepted'] = array();
						}
							
						$availMap[$acceptedDate]['accepted'][] = $participant->id;
					}
				}
			}
		}

		reset($availMap);

		$availability['availMap'] = $availMap;

		return $availability;
	}
}
